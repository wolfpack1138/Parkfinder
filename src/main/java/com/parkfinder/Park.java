package com.parkfinder;

import java.util.Comparator;

public class Park {
	
	private String name;
	private double rating=0.0;
	private String vicinity;
	private double distance;
	
	Park(String name, double rating, String vicinity, double distance){
		setName(name);
		setRating(rating);
		setVicinity(vicinity);
		setDistance(distance);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public double getRating() {
		return this.rating;
	}
	
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	public String getVicinity() {
		return this.vicinity;
	}
	
	public void setVicinity(String vicinity) {
		this.vicinity=vicinity;
	}
	

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "Park [name=" + name + ", rating=" + rating + ", vicinity=" + vicinity + ", distance=" + distance + "]";
	}
	
	public static final Comparator<Park> distOrderAsc = new Comparator<Park>() {
		@Override
		public int compare(Park o1, Park o2) { 
			if(o1.getDistance()>o2.getDistance())
				return 1;
			if(o1.getDistance()<o2.getDistance()) 
				return -1;
			return 0;
		}
	};
}
