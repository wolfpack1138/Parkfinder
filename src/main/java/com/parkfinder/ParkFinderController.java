package com.parkfinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Controller
public class ParkFinderController {

	//Google Places Api Key
	private final String gPlacesApiKey = "AIzaSyCNF7Ap2mmJsQ_tzFKvS6AY3EmysooamEE";
	//Google Maps Api Key
	private final String gMapsApiKey = "AIzaSyDqfaiwDVk3l2e5j8xe0Ft5tLY0zrC6IyU";
	//Google Maps Api Url
	private final String gMapsUrl = "https://maps.googleapis.com/maps/api/geocode/json?";
	//Google Places Api Url
	private final String gPlacesUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
	
	//# of meters in 10 miles
	final double mtrsInTenMiles = 1609*10;
	final double pi = Math.PI;

	
	/**
	 * This method 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)	
	public ModelAndView parks() {
		return new ModelAndView("parks", "command", new Location());
	}
	
	/**
	 * This method will find the parks and order them by the lowest distance 
	 * @param location
	 * @return
	 */
	
	@RequestMapping(value="/findParks", method=RequestMethod.POST)
	public ModelAndView findParks(HttpServletRequest request){
		Location location;	
		try {
			//
			String lat = request.getParameter("latitude");
			String lng = request.getParameter("longitude");
			String street = request.getParameter("streetAddress");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String country = request.getParameter("country");
		
			double latitude = Double.parseDouble(lat);
			double longitude = Double.parseDouble(lng);
		
			
			location = new Location(street,city,state,country,latitude,longitude);
		}
		
		catch(NumberFormatException e) {
			return new ModelAndView("error","message","Latitude and/or Longitude is invalid");
		}
		
	
		List<Park> parks= new ArrayList<>();
		ModelAndView model = new ModelAndView("parkresults");
		if(location.getLatitude()!=0.0 ||location.getLongitude()!=0.0 ) {						//if user entered location using latitude and longitude
			parks=findParksByLatLong(location.getLatitude(),location.getLongitude());
			if(parks.isEmpty()||parks.size()==0) {
				return new ModelAndView("error","message","Zero Results Returned");
			}
			parks.sort(Park.distOrderAsc);
			model.addObject("parks",parks);
		}
		else
		{
			if(location.getStreetAddress()==null || location.getStreetAddress().isEmpty()) {		//Url requires there be an address so if none, the process cannot proceed
				return new ModelAndView("error","message","Please Go back and enter an address");
			}
			String[] s = {location.getStreetAddress(),location.getCity(),location.getState(),location.getCountry()};
			StringBuilder strbdr = new StringBuilder();
			for(String str: s) {
				if(str==null) {
					continue;
				}
				strbdr.append(str.replace(" ", "+")).append(",");
			}
			
			double[] latlng = getLatitudes(strbdr.toString());
			if (latlng==null) {	
				return new ModelAndView("error","message","There was an issue with your address. Please go back and try again.");
			}
			parks = findParksByLatLong(latlng[0],latlng[1]);
			if(parks.isEmpty()||parks.size()==0) {
				return new ModelAndView("error","message","Zero Results Returned");
			}
			parks.sort(Park.distOrderAsc);
			model.addObject("parks",parks);
			
		}
		
		return model;
		
	}

	
	/**
	 * This method uses the longitude and latitude of the location of the user and makes a GET request using the Google Places API Url
	 * If successful and contains > 0 results, this method will return the parks within a 10 mile radius 
	 * @param lat
	 * @param lng
	 * @return
	 */
	List<Park> findParksByLatLong(double lat, double lng){
		try {
			String apiKey = "&key="+gPlacesApiKey;
			String url = gPlacesUrl+"location="+lat+","+lng+"&radius="+mtrsInTenMiles+"&type=park"+apiKey;
			List<Park> parks = new ArrayList<>();

			RestTemplate r = new RestTemplate();
			ResponseEntity<String> rep = r.getForEntity(url, String.class);
			String obj = rep.getBody();
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject) parser.parse(obj);
			
			JsonArray arr = json.getAsJsonArray("results");	
			if(arr==null || arr.size()==0) {					//If the results returned are 0 return empty list 
				return Collections.emptyList();
			}
			Iterator<JsonElement> iterator = arr.iterator();
			while(iterator.hasNext()) {
				JsonObject p = iterator.next().getAsJsonObject();
				String name = p.get("name").getAsString();
				double rating = ((p.get("rating")==null)? 0.0 : p.get("rating").getAsDouble());
				String vicinity =((p.get("vicinity")==null)? "N/A" : p.get("vicinity").getAsString());
				
				JsonObject location = p.getAsJsonObject("geometry").getAsJsonObject("location");
				double lat1 = location.get("lat").getAsDouble();
				double long1 = location.get("lng").getAsDouble();
				double dist = getDistance(lat,lng,lat1,long1);					//returns the distance of the park from the user location
				Park park = new Park(name,rating,vicinity,dist);
				parks.add(park);
			}
			
			return parks;

		}
		
		catch(Exception e) {
			e.printStackTrace();
			return Collections.emptyList();
		}

	}
	
	/**
	 * This method calculates the distance between 2 coordinates (Latitude and Longitude)
	 * @param lat1
	 * @param long1
	 * @param lat2
	 * @param long2
	 * @return
	 */
	public double getDistance(double lat1, double long1, double lat2, double long2) {
		 double d = long1 - long2;
		 double dist = Math.sin(degToRads(lat1)) * Math.sin(degToRads(lat2)) +
				  		Math.cos(degToRads(lat1)) * Math.cos(degToRads(lat2)) * Math.cos(degToRads(d)); 
		  return radToDegs(Math.acos(dist)) * 60 * 1.1515;
	}
		
		private double degToRads(double deg) {
		  return (deg * pi / 180.0);
		}

		private double radToDegs(double rad) {
		  return (rad * 180.0 / pi);
		}

	
	/**
	 * This method appends the user entered address to the Google Maps API url and makes a 
	 * GET request on that url. If successful and contains > 0 results, this method will 
	 * return the latitude and longitude in an array
	 * @param address
	 * @return
	 */
	double[] getLatitudes(String address) {
		String apiKey = "key="+gMapsApiKey;
		String url = gMapsUrl+"address="+address+"&"+apiKey;		//complete url of the request
		
		try {
		
			RestTemplate r = new RestTemplate();
			ResponseEntity<String> rep = r.getForEntity(url, String.class);
			String obj = rep.getBody();
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject) parser.parse(obj);		
			
			JsonArray res = json.getAsJsonArray("results");
			if (res.size()==0) {		//if address is bad and the results returned = 0 
				return null;
			}
			else {
				JsonObject loc= res.get(0).getAsJsonObject().getAsJsonObject("geometry").getAsJsonObject("location");
				double[] d = {loc.get("lat").getAsDouble(),loc.get("lng").getAsDouble()};
				
				return d;				
					
			}

		}
		
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
