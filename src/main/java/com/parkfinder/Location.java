package com.parkfinder;

public class Location{
	
	private String streetAddress;
	private String city;
	private String state;
	private String country;
	private double latitude;
	private double longitude;
	
	
	Location(){};
	
	Location(String street, String city, String state, String country, double lat, double lng){
		setStreetAddress(street);
		setCity(city);
		setState(state);
		setCountry(country);
		setLatitude(lat);
		setLongitude(lng);
		
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Override
	public String toString() {
		return "Location [streetAddress=" + streetAddress + ", city=" + city + ", state=" + state + ", country="
				+ country + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
	
	
	

}
