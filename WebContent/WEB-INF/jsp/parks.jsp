<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
   <head>
      <title>ParkFinder 1.0</title>
   </head>

   <body>
      <h2>Location Information</h2>
      <form:form method = "POST" action = "/pf/findParks">
         <table>
            <tr>
               <td><form:label path = "streetAddress">Address</form:label></td>
               <td><form:input path = "streetAddress"/></td>
            </tr>
            <tr>
               <td><form:label path = "city">City</form:label></td>
               <td><form:input path = "city" /></td>
            </tr>
            
            <tr>
               <td><form:label path = "state">State</form:label></td>
               <td><form:input path = "state" /></td>
            </tr>
            
            <tr>
               <td><form:label path = "country">Country</form:label></td>
               <td><form:input path = "country" /></td>
            </tr>          
          </table> 
          <heading> OR </heading>  
          <table>              

            <tr>
               <td><form:label path = "latitude">Latitude</form:label></td>
               <td><form:input path = "latitude" /></td>
            </tr>         
            
            <tr>
               <td><form:label path = "longitude">Longitude</form:label></td>
               <td><form:input path = "longitude" /></td>
            </tr>
          </table>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>

      </form:form>
   </body>
   
</html>