<%@page contentType = "text/html;charset = UTF-8" language = "java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<html>
   <head>
      <title>ParkFinder 1.0</title>
   </head>


   <body>
		<h1>ParkFinder</h1>
		<table border="1px solid black">
			<th>Park Name</th>
			<th>Rating</th>
			<th>Vicinity</th>
			<th>Distance</th>
			<c:forEach items="${parks}" var="park" >
				<tr>
					<td>${park.name}</td>
					<td>${park.rating}</td>
					<td>${park.vicinity}</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${park.distance}"/> mi</td>
				</tr>
			</c:forEach>
		</table>
   </body>
   
</html>